import zmq
import time
import sys

context = zmq.Context()
sender = context.socket(zmq.DEALER)
sender.bind('tcp://127.0.0.1:11111')

message = 'Hi'

print "Start at", time.time()
for i in xrange(int(sys.argv[1])):
    sender.send(message)