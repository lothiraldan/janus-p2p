import zmq
import time
import sys

context = zmq.Context()
sender = context.socket(zmq.DEALER)
sender.connect('tcp://127.0.0.1:11111')

print "Ready", time.time()
i = 0
while i < int(sys.argv[1]):
    sender.recv()
    i += 1
print "End at", time.time()