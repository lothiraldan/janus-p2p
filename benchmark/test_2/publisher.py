import zmq
import time
import sys

context = zmq.Context()
publisher = context.socket(zmq.PUB)
publisher.bind('tcp://127.0.0.1:11112')

publisher.send('')
time.sleep(2)

message = ['sample', 'Hi']

print "Start at", time.time()
for i in xrange(int(sys.argv[1])):
    publisher.send_multipart(message)
print "End at", time.time()