import zmq
import time

def publisher(port, messages, agents_count, queue):
    context = zmq.Context()
    publisher = context.socket(zmq.PUB)
    publisher.bind('tcp://127.0.0.1:%s' % port)

    time.sleep(1)
    publisher.send('')

    message = 'Hi'

    start = time.time()
    queue.put(start)
    for i in xrange(messages/agents_count):
        for j in xrange(agents_count):
            publisher.send_multipart((str(j), message))
    end = time.time()