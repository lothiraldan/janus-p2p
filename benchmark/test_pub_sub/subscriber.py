import zmq
import time
import sys


def subscriber(port, min_agent_id, max_agent_id, messages, queue):
    context = zmq.Context()
    subscriber = context.socket(zmq.SUB)
    for agent_id in xrange(min_agent_id, max_agent_id):
        subscriber.setsockopt(zmq.SUBSCRIBE, str(agent_id))
    subscriber.connect('tcp://127.0.0.1:%s' % port)

    i = 0
    while i < messages:
        subscriber.recv_multipart()
        i += 1
    end = time.time()
    queue.put(end)