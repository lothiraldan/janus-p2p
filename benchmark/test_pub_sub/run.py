import time
import os
import random
from publisher import publisher
from subscriber import subscriber
from multiprocessing import Process, Queue
from collections import defaultdict
import json
import signal

kernels = [1, 2, 5, 10, 20, 50]
agents_per_kernel = [1, 2, 5, 10, 20, 50, 100]

results = []

TIMES = 5

class TimeoutException(Exception):
    pass

def run(kernels, agents_per_kernel, messages):
    print locals()

    queue = Queue()

    processes = []
    port = random.randint(10000, 20000)
    for i, kernel_count in enumerate(range(kernels)):
        kernel_min_id = i*agents_per_kernel
        kernel_max_id = ((i+1)*agents_per_kernel)
        # print "Subscriber args", (port, kernel_min_id, kernel_max_id, messages/kernels)
        processes.append(Process(
            target=subscriber,
            args=(port, kernel_min_id, kernel_max_id, messages/kernels, queue)))

    for p in processes:
        p.start()

    pub = Process(target=publisher, args=(port, messages, kernels*agents_per_kernel, queue))
    pub.start()

    started = queue.get()
    print "Started at", started

    stopped = 0

    try:
        for p in processes:
            stopped = max(stopped, queue.get())
            p.join()
            processes.pop(0)
        pub.join()
    except TimeoutException as e:
        for p in processes:
            try:
                os.kill(p.pid, 2)
            except OSError:
                pass
        try:
            os.kill(pub.pid, 2)
        except OSError:
            pass
        raise e

    print "Stopped at", stopped
    return messages / (stopped - started)


def timeout_handler(signum, frame):
    raise TimeoutException()
signal.signal(signal.SIGALRM, timeout_handler)

for i, k in enumerate(kernels):
    for a in agents_per_kernel:
        times = []
        for l in xrange(TIMES):
            print "Loop"
            run_ok = False
            while not run_ok:
                try:
                    signal.alarm(12) # triger alarm in 12 seconds
                    times.append(run(k, a, 100000))
                    run_ok = True
                    signal.alarm(0)
                except TimeoutException:
                    continue
        results.append((a, k, sum(times)/TIMES*1.))

print "SAVING"
with open('data.bench', 'w') as f:
    json.dump(results, f)
print "SAVE"