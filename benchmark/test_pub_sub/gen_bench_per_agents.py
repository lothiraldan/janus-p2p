import json
from collections import defaultdict
import matplotlib.pyplot as plt
from operator import itemgetter

with open('data.bench') as f:
    data = json.load(f)

results = defaultdict(dict)
agents = set()

for item in data:
    k, a, mps = item
    agents.add(a)
    results[k][a] = mps

plt.figure(figsize=(10, 8))
x_series = sorted(list(agents))

series = []
for kernel, kernel_serie in results.items():
    serie = [x[1] for x in sorted(kernel_serie.items(), key=itemgetter(0))]
    series.append((kernel, serie))

series = sorted(series, key=itemgetter(0))
for kernel, serie in series:
    plt.plot(x_series, serie, label="%s kernels" % kernel)

#create legend
plt.legend(loc="upper right")
plt.xlabel("Nombre d'agent par kernel")
plt.ylabel("Message par secondes")
plt.ylim(11000, 30000)
plt.title("Benchmark de ZeroMQ par nombre d'agents")

#save figure to png
plt.savefig("bench_per_agents.png")