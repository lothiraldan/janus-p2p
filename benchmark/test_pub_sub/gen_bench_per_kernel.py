import json
from collections import defaultdict
import matplotlib.pyplot as plt
from operator import itemgetter

with open('data.bench') as f:
    data = json.load(f)

results = defaultdict(dict)
kernels = set()

for item in data:
    k, a, mps = item
    kernels.add(k)
    results[a][k] = mps

plt.figure(figsize=(10, 8))
x_series = sorted(list(kernels))

series = []
for agent, agent_serie in results.items():
    serie = [x[1] for x in sorted(agent_serie.items(), key=itemgetter(0))]
    series.append((agent, serie))

series = sorted(series, key=itemgetter(0))
for agent, serie in series:
    plt.plot(x_series, serie, label="%s agents par kernels" % agent)

#create legend
plt.legend(loc="upper right")
plt.xlabel("Nombre de kernels")
plt.ylabel("Message par secondes")
plt.ylim(11000, 30000)
plt.title("Benchmark de ZeroMQ par nombre de kernels")

#save figure to png
plt.savefig("bench_per_kernel.png")