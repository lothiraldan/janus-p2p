import zmq
import time
import sys

context = zmq.Context()
publisher = context.socket(zmq.PUB)
publisher.bind('tcp://127.0.0.1:11112')

publisher.send('')
time.sleep(2)

message = ['sample1', 'Hi']
message2 = ['sample2', 'Hi']

print "Start at", time.time()
for i in xrange(int(sys.argv[1])/2):
    publisher.send_multipart(message)
    publisher.send_multipart(message2)
print "End at", time.time()