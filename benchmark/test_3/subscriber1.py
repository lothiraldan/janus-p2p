import zmq
import time
import sys

context = zmq.Context()
subscriber = context.socket(zmq.SUB)
for i in xrange(int(sys.argv[2]), int(sys.argv[3])):
    subscriber.setsockopt(zmq.SUBSCRIBE, str(i))
subscriber.connect('tcp://127.0.0.1:%s' % sys.argv[1])

print "Subscriber ready", time.time()
i = 0
while i < int(sys.argv[4]):
    subscriber.recv_multipart()
    i += 1
print "Subscriber end at", time.time()