import zmq
import time
import sys

context = zmq.Context()
subscriber = context.socket(zmq.SUB)
subscriber.setsockopt(zmq.SUBSCRIBE, 'sample1')
subscriber.setsockopt(zmq.SUBSCRIBE, 'sample2')
subscriber.connect('tcp://127.0.0.1:11112')

print "Ready", time.time()
i = 0
while i < int(sys.argv[1]):
    subscriber.recv_multipart()
    i += 1
print "End at", time.time()