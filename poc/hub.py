import zmq
import logging

HUB_SUB_PORT = 12345
HUB_PUB_PORT = 12346
HUB_RCV_PORT = 12347
HUB_SND_PORT = 12348

# Prepare our context and sockets
context = zmq.Context()
rcv = context.socket(zmq.ROUTER)
rcv.bind("tcp://*:%s" % HUB_RCV_PORT)
snd = context.socket(zmq.ROUTER)
snd.bind("tcp://*:%s" % HUB_SND_PORT)
sub = context.socket(zmq.SUB)
sub.setsockopt(zmq.SUBSCRIBE, '')
sub.bind("tcp://*:%s" % HUB_SUB_PORT)
pub = context.socket(zmq.PUB)
pub.bind("tcp://*:%s" % HUB_PUB_PORT)

# Initialize poll set
poller = zmq.Poller()
poller.register(rcv, zmq.POLLIN)
poller.register(snd, zmq.POLLIN)
poller.register(sub, zmq.POLLIN)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

logger.info('Start logger')

# Switch messages between sockets
while True:
    socks = dict(poller.poll())

    if socks.get(rcv) == zmq.POLLIN:
        message = rcv.recv_multipart()
        logger.info("Message in rcv: %s", message)
        if message[-1] == 'ping':
            rcv.send_multipart((message[0], '', 'pong'))
            logger.info("Get ping, send pong")
            continue
        snd.send_multipart(message[1:])

    if socks.get(snd) == zmq.POLLIN:
        message = snd.recv_multipart()
        logger.info("Message in snd: %s", message)
        rcv.send_multipart(message[1:])

    if socks.get(sub) == zmq.POLLIN:
        msg = sub.recv_multipart()
        logger.info("Message in sub: %s", msg)
        pub.send_multipart(msg)
