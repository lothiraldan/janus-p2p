import zmq
import sys
import json

c = zmq.Context()
s = c.socket(zmq.DEALER)
s.connect('tcp://127.0.0.1:%s' % sys.argv[1])

while True:
    cmd = raw_input('Enter cmd: ').split(' ', 1)
    s.send_multipart(('', cmd[0], json.dumps({'msg': cmd[1]})))
