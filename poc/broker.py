import zmq
import json
import socket
import logging
import threading

logging.basicConfig(level=logging.DEBUG)

# Socket part
ANY = "0.0.0.0"
MCAST_ADDR = "237.252.249.227"
MCAST_PORT = 1600
#create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
#allow multiple sockets to use the same PORT number
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#Bind to the port that we know will receive multicast data
sock.bind((ANY, MCAST_PORT))
#tell the kernel that we are a multicast socket
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)
#Tell the kernel that we want to add ourselves to a multicast group
#The address for the multicast group is the third param
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP,
    socket.inet_aton(MCAST_ADDR) + socket.inet_aton(ANY))

stop_event = threading.Event()
started_event = threading.Event()


def run(blocking=True):
    logging.info('Multicast broker started on tcp://127.0.0.1:32001')
    if blocking:
        sock.settimeout(1)
    else:
        sock.settimeout(0)

    # zeromq part
    pub = zmq.Context.instance().socket(zmq.PUB)
    pub.bind('tcp://127.0.0.1:32001')

    stop_event.clear()
    while not stop_event.is_set():
        started_event.set()
        try:
            data, addr = sock.recvfrom(1024)
            data = json.loads(data)
            data.update({'address': addr[0]})
            # data.update({'address': '127.0.0.1'})
            logging.info('Get data %s', data)
        except socket.error:
            pass
        else:
            pub.send('register %s' % json.dumps(data))
            del data, addr

    logging.info('Multicast broker stopped')


def stop():
    stop_event.set()

if __name__ == '__main__':
    run()
