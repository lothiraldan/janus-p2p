# -*- coding: utf-8 -*-
import zmq
import uuid
import json
import socket
import random
import logging
from time import sleep
from random import randint
from datetime import timedelta
from hashlib import sha1

from zmq.eventloop import ioloop, zmqstream

logging.basicConfig(level=logging.DEBUG)

# Come from excellent http://stefan.sofa-rockers.org/2012/02/01/designing-and-testing-pyzmq-applications-part-1/
def stream(context, loop, sock_type, addr, bind, callback=None, subscribe=None,
        random=False, identity=None):
    """
    Creates a :class:`~zmq.eventloop.zmqstream.ZMQStream`.

    :param sock_type: The ØMQ socket type (e.g. ``zmq.REQ``)
    :param addr: Address to bind or connect to formatted as *host:port*,
            *(host, port)* or *host* (bind to random port).
            If *bind* is ``True``, *host* may be:

            - the wild-card ``*``, meaning all available interfaces,
            - the primary IPv4 address assigned to the interface, in its
              numeric representation or
            - the interface name as defined by the operating system.

            If *bind* is ``False``, *host* may be:

            - the DNS name of the peer or
            - the IPv4 address of the peer, in its numeric representation.

            If *addr* is just a host name without a port and *bind* is
            ``True``, the socket will be bound to a random port.
    :param bind: Binds to *addr* if ``True`` or tries to connect to it
            otherwise.
    :param callback: A callback for
            :meth:`~zmq.eventloop.zmqstream.ZMQStream.on_recv`, optional
    :param subscribe: Subscription pattern for *SUB* sockets, optional,
            defaults to ``b''``.
    :returns: A tuple containg the stream and the port number.

    """
    sock = context.socket(sock_type)

    # Bind/connect the socket
    if bind:
        if not random:
            sock.bind(addr)
        else:
            port = sock.bind_to_random_port(addr)
    else:
        sock.connect(addr)

    # Add a default subscription for SUB sockets
    if sock_type == zmq.SUB:
        if subscribe:
            sock.setsockopt(zmq.SUBSCRIBE, subscribe)
        else:
            sock.setsockopt(zmq.SUBSCRIBE, '')

    # Identity
    if identity:
        sock.setsockopt(zmq.IDENTITY, identity)

    # Create the stream and add the callback
    stream = zmqstream.ZMQStream(sock, loop)
    if callback:
        stream.on_recv(callback)

    if random:
        return stream, int(port)
    else:
        return stream


class Service(object):

    # Multicast/Local registration
    ANY = "0.0.0.0"
    SENDERPORT = 32000
    MCAST_ADDR = "237.252.249.227"
    MCAST_PORT = 1600

    # Global registration
    HUB_HOST = "172.18.20.158"
    HUB_SUB_PORT = 12345
    HUB_PUB_PORT = 12346
    HUB_RCV_PORT = 12347
    HUB_SND_PORT = 12348

    def __init__(self):
        # Service
        self.service_id = str(uuid.uuid4())
        self.services = {}
        self.logger = logging.getLogger(self.service_id)

        # Delegate
        self.delegate = None
        self.fqdn = None

        # Sockets
        self.context = zmq.Context.instance()
        self.loop = ioloop.IOLoop.instance()

        # Hub
        self.sended_msg = set()

        self.pub, self.pub_port = stream(self.context, self.loop, zmq.PUB,
            'tcp://*', bind=True, random=True)
        self.sub = stream(self.context, self.loop, zmq.SUB,
            'tcp://127.0.0.1:32001', bind=False, callback=self.process_sub)
        self.server, self.server_port = stream(self.context, self.loop,
            zmq.DEALER, 'tcp://*', bind=True, random=True,
            callback=self.process_server, identity=self.service_id)

        self.logger.info('Start %s, listen to %s and pub to %s' %
            (self.service_id, self.server_port, self.pub_port))

    def register(self):
        self.register_local()
        self.loop.add_timeout(timedelta(seconds=3),
            lambda: self.loop.stop())
        self.loop.start()
        self.register_global()

    def register_local(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
            socket.IPPROTO_UDP)
        sock.bind((self.ANY, 0))
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 255)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        sock.sendto(json.dumps(self._register_info()),
            (self.MCAST_ADDR, self.MCAST_PORT))

    def register_global(self):
        # Check if we have a delegate
        if self.delegate:
            return

        self.logger.info('No delegate, wait and try to contact HUB')
        # sleep(randint(1, 10))
        req = self.context.socket(zmq.REQ)
        address = 'tcp://%s:%s' % (self.HUB_HOST, self.HUB_RCV_PORT)
        req.connect(address)
        req.send('ping')

        poll = zmq.Poller()
        poll.register(req, zmq.POLLIN)

        socks = dict(poll.poll(5 * 1000))
        if not socks.get(req, None) == zmq.POLLIN:
            self.logger.info('Couldn\'t contact hub, abort')
            return

        self.delegate = True
        self.fqdn = self.service_id
        self.logger.info('Could contact hub, register')

        self.hub_sub = stream(self.context, self.loop, zmq.SUB,
            'tcp://%s:%s' % (self.HUB_HOST, self.HUB_PUB_PORT), bind=False,
            callback=self.process_hub_sub)
        self.hub_pub = stream(self.context, self.loop, zmq.PUB,
            'tcp://%s:%s' % (self.HUB_HOST, self.HUB_SUB_PORT), bind=False)
        self.server.connect('tcp://%s:%s' % (self.HUB_HOST, self.HUB_SND_PORT))

        sleep(1)
        info = self._register_info()
        info['address'] = 'hub'
        self.hub_publish('register %s' % json.dumps(info))

        info['delegate'] = True
        self.pub.send('register %s' % json.dumps(info))

    def _register_info(self):
        data = {'id': self.service_id, 'pub_port': self.pub_port,
            'server_port': self.server_port,
            'fqdn': self.fqdn}
        return data

    def process_sub(self, messages):
        for message in messages:
            self.logger.info('Process sub: %s' % (message))
            if self.process_sub_msg(message) and self.delegate is True:
                message_type, data = message.split(' ', 1)
                data = json.loads(data)
                self.forward_sub_to_hub(message)

    def process_hub_sub(self, messages):
        for message in messages:
            self.logger.info('Process hub sub: %s' % (message))
            msg_hash = sha1(message).hexdigest()

            if msg_hash in self.sended_msg:
                self.logger.info('Drop message')
                continue

            self.process_sub_msg(message)
            self.pub.send(message)

    def forward_sub_to_hub(self, message):
        message_type, data = message.split(' ', 1)
        data = json.loads(data)

        if message_type == 'register':
            data['address'] = 'hub'
        self.hub_publish(' '.join((message_type, json.dumps(data))))

    def process_sub_msg(self, message):
        message_type, data = message.split(' ', 1)
        data = json.loads(data)

        if message_type == 'register' and data['id'] != self.service_id:

            register_to = False
            if data['id'] not in self.services:
                register_to = True

            self.process_register(data)
            if register_to:
                self.register_to(data['id'], data['address'])

            if not data['fqdn']:
                return False

        return True

    def process_server(self, messages):
        self.logger.info('Process raw message %s', messages)
        if messages[0] != '':
            if self.delegate is True:
                self.forward_message(messages)
                return
            else:
                # Not for me and I'm not a hub
                return

        message_type, data = messages[-2:]
        data = json.loads(data)

        self.logger.info('Process server: %s/%s' % (message_type, data))

        if message_type == 'register' and data['id'] != self.service_id:
            self.process_register(data)
        elif message_type == 'send':
            self.send(random.choice(self.services.keys()), data['msg'])
        elif message_type == 'publish':
            self.publish(data['msg'])
            if self.delegate is True:
                self.hub_publish('msg %s' % json.dumps(data['msg']))
        else:
            self.logger.info('Get message: %s', data)

    def forward_message(self, message):
        index = message.index('')
        destination, content = message[:index], message[index:]
        print "Content", content
        self.send(destination, json.loads(content[-1]),
            type=content[-2])

    def register_to(self, service_id, remote_address):
        register_info = self._register_info()
        if remote_address == 'hub':
            register_info['address'] = 'hub'
        else:
            register_info['address'] = socket.gethostbyname(socket.gethostname())
            if self.delegate is True:
                register_info['delegate'] = True
        self.send(service_id, register_info, type='register')

    def process_register(self, data):
        if data['id'] in self.services:
            self.logger.info('Known node, update only')
            data['address'] = self.services[data['id']]['address']
            self.services[data['id']] = data
            return

        self.services[data['id']] = data

        if data['address'] != 'hub':
            sub_address = 'tcp://%s:%s' % (data['address'], data['pub_port'])
            self.logger.info('Connect sub to %s' % sub_address)
            self.sub.connect(sub_address)

            if data.get('delegate', False):
                self.delegate = data
                old_fqdn = self.fqdn
                self.fqdn = '%s.%s' % (data['id'], self.service_id)
                self.logger.info('My FQDN is now: %s' % self.fqdn)
                self.logger.info('Delegate is now: %s' % (data))
                if old_fqdn is None:
                    self.pub.send('register %s' % json.dumps(self._register_info()))
        else:
            if self.delegate is True:
                # Send to local nodes
                self.pub.send('register %s' % json.dumps(data))

        self.logger.info('Service %s [%s] is at %s:%s, publish at %s:%s ' % (data['id'], data['fqdn'],
            data['address'], data['server_port'], data['address'],
            data['pub_port']))

    def start(self):
        self.loop.start()

    def send(self, destination, msg, type='msg'):
        self.logger.info('Send %s to %s' % (msg, destination))
        if isinstance(destination, list):
            main_destination = destination[0]
            rest = destination[1:]
        else:
            main_destination = destination
            rest = []
        data = self.services[main_destination]
        self.logger.info('Destination info: %s' % data)

        client = self.context.socket(zmq.DEALER)
        if data['address'] == 'hub':
            if self.delegate is True:
                address = 'tcp://%s:%s' % (self.HUB_HOST, self.HUB_RCV_PORT)
            else:
                address = 'tcp://%s:%s' % (self.delegate['address'],
                    self.delegate['server_port'])
            client.connect(address)
            frames = str(data['fqdn']).split('.')
            if rest:
                frames += rest
            frames += ('', type, json.dumps(msg))
            self.logger.info('Send frames: %s' % frames)
            client.send_multipart(frames)
        else:
            address = 'tcp://%s:%s' % (data['address'], data['server_port'])
            client.connect(address)
            client.send_multipart(('', type, json.dumps(msg)))

    def publish(self, msg):
        msg = str(json.dumps(msg))
        self.pub.send('msg %s' % msg)

    def hub_publish(self, msg):
        msg_id = sha1(msg).hexdigest()
        self.sended_msg.add(msg_id)
        self.hub_pub.send(msg)

if __name__ == '__main__':
    s = Service()
    s.register()
    s.start()
